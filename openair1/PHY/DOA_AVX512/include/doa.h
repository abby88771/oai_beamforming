#ifndef __DOA_H
#define __DOA_H
// void MUSIC_DOA_2A(int M, int qr_iter, float *angle, int number_angle, float *result, int SNR);
// void MVDR_DOA_2A(int M, int qr_iter, float *angle, int number_angle, float *result, int SNR);
// extern void origin_doa_function(PHY_VARS_gNB *gNB, float IQ_real, float IQ_imag);

#include "PHY/defs_gNB.h"
void avx512_doa_function(PHY_VARS_gNB *gNB, float IQ_real, float IQ_imag);

#ifdef __cplusplus
extern "C" {
#endif
void origin_doa_function(float IQ_real, float IQ_imag);
#ifdef __cplusplus
} // extern "C"
#endif

#endif