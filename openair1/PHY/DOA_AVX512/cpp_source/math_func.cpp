// C++
#define PI acos(-1)
extern "C" {
#include "math_func.h"
#include "color.h"
}
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <random>
#include <ccomplex>
using namespace std;
// g++ -c  math_func.cpp -Wall -Wextra -std=c++14
// ar -rcs math_func.a math_func.o
// g++ -o test test.c math_func.a

complex<float> I_1(0, 1);
complex<double> I_d(0, 1);
static complex<double> imag_value = {0, 1};

void cpp_abs(float *Real, float *Imaginary, float *result)
{
  complex<double> complex_variable = {*Real, *Imaginary};
  *result = abs(complex_variable);
  // printf("abs|%.2f,%.2f| = %.4f\n", *Real, *Imaginary, *result);
}

void cpp_sqrt(float *Real, float *Imaginary)
{
  complex<double> complex_variable = {*Real, *Imaginary};
  // cout << "Square root of ";
  // cout << complex_variable;
  // cout << " = ";
  // cout << sqrt(complex_variable) << endl;

  *Real = sqrt(complex_variable).real();
  *Imaginary = sqrt(complex_variable).imag();

  // printf("Real = %.4f, Imaginary = %.4f\n", *Real, *Imaginary);
}

void cpp_division(float *Re_a, float *Im_b, float *Re_c, float *Im_d)
{
  complex<double> complex_variable_ab = {*Re_a, *Im_b};
  complex<double> complex_variable_cd = {*Re_c, *Im_d};
  // cout << "Division of ";
  // cout << complex_variable_ab;
  // cout << " /= ";
  // cout << complex_variable_cd;
  // cout << " = ";
  // cout << (complex_variable_ab / complex_variable_cd) << endl;
  complex_variable_ab /= complex_variable_cd;
  *Re_a = complex_variable_ab.real();
  *Im_b = complex_variable_ab.imag();
  // printf("Real = %.4f, Imaginary = %.4f\n", *Re_a, *Im_b);
}

void cpp_division2(float Re_a, float Im_b, float *Re_c, float *Im_d, float *result_real, float *result_imag)
{
  complex<double> complex_variable_ab = {Re_a, Im_b};
  complex<double> complex_variable_cd = {*Re_c, *Im_d};
  complex<double> complex_variable_ef; // result store here
  // cout << "Division2 of ";
  // cout << complex_variable_ab;
  // cout << " / ";
  // cout << complex_variable_cd;
  // cout << " = ";
  // cout << (complex_variable_ab / complex_variable_cd) << endl;
  complex_variable_ef = complex_variable_ab / complex_variable_cd;
  *result_real = complex_variable_ef.real();
  *result_imag = complex_variable_ef.imag();
  // printf("Real = %.4f, Imaginary = %.4f\n", *result_real, *result_imag);
}
void cpp_division3(float *Re_a, float *Im_b, float *Re_c, float *Im_d)
{
  complex<double> complex_variable_ab = {*Re_a, *Im_b};
  complex<double> complex_variable_cd = {*Re_c, *Im_d};
  // cout << "Division3 of ";
  // cout << complex_variable_ab;
  // cout << " / ";
  // cout << complex_variable_cd;
  // cout << " = ";
  // cout << (complex_variable_ab / complex_variable_cd) << endl;

  *Re_c = (complex_variable_ab / complex_variable_cd).real();
  *Im_d = (complex_variable_ab / complex_variable_cd).imag();
  // printf("Real = %.4f, Imaginary = %.4f\n", *Re_c, *Im_d);
}

std::complex<double> cpp_randn(void)
{
  std::random_device randomness_device{};
  std::mt19937 pseudorandom_generator{randomness_device()};
  auto mean = 0.0;
  auto std_dev = 1.0;
  std::normal_distribution<> distribution{mean, std_dev};
  auto sample = distribution(pseudorandom_generator);
  return (std::complex<double>)(sample);
}

void cpp_awgn(float *input_re, float *input_im, float *output_re, float *output_im, int snr, int row, int col)
{
  printf("cpp_awgn(%d,%d)\n", row, col);
  complex<double> *input_signal = (complex<double> *)malloc(row * col * sizeof(complex<double>)); // Input
  complex<double> *output_signal = (complex<double> *)malloc(row * col * sizeof(complex<double>)); // Output

  complex<double> Esym;
  complex<double> No;
  complex<double> noiseSigma;
  complex<double> n;
  complex<double> e_AWGN;
  for (int i = 0; i < row * col; i++) {
    input_signal[i] = {input_re[i], input_im[i]};
    output_signal[i] = {output_re[i], output_im[i]};
    //---------------------------------------------------------------
    Esym += pow(abs(input_signal[i]), 2) / complex<double>(row * col);
    No = Esym / complex<double>(snr);
    noiseSigma = sqrt(No / complex<double>(2));
    n = noiseSigma * (cpp_randn() + cpp_randn() * imag_value);
    output_signal[i] = input_signal[i] + n;
    // e_AWGN += output_signal[i]; //debug mode
    //---------------------------------------------------------------
    output_re[i] = output_signal[i].real();
    output_im[i] = output_signal[i].imag();
    // std::cout << "---awgn output_signal---" << output_signal[i] << std::endl;
  }
  // std::cout << "---awgn sum ---" << e_AWGN << std::endl;
  // std::cout << "---awgn average.real ---" << e_AWGN.real() / (row * col) << std::endl;
  // std::cout << "---awgn average.imag ---" << e_AWGN.imag() / (row * col) << std::endl;
}

void cpp_exp(float *A_theta_re, float *A_theta_im, float *t_theta, float d, float kc, int i, int j)
{
  complex<float> A_theta;
  A_theta = exp(I_1 * kc * std::complex<float>(i) * d * sin((*t_theta) * complex<float>(PI / 180)));
  // std::cout << "---A_theta---" << A_theta;

  *A_theta_re = A_theta.real();
  *A_theta_im = A_theta.imag();
}
void cpp_exp2(float *a_vector_re, float *a_vector_im, float *dr, float d, float kc, int i, int j)
{
  complex<float> a_vector;
  a_vector = exp(I_1 * kc * (complex<float>)j * d * sin(dr[i]));
  // std::cout << "---A_theta---" << A_theta;
  *a_vector_re = a_vector.real();
  *a_vector_im = a_vector.imag();
  // cout << "---a_vector---" << a_vector << endl;
}

void cpp_t_sig(float *t_sig_re, float *t_sig_im)
{
  complex<double> t_sig;
  t_sig = (cpp_randn() + cpp_randn() * I_d) / complex<double>(sqrt(2));
  // std::cout << "---t_sig---" << t_sig << std::endl;

  *t_sig_re = t_sig.real();
  *t_sig_im = t_sig.imag();
}

float cpp_20log_abs(float *S_MUSIC_re, float *S_MUSIC_im)
{
  complex<double> S_MUSIC = {*S_MUSIC_re, *S_MUSIC_im};
  // std::cout << "20log" << S_MUSIC << " = " << 20 * log10(abs(S_MUSIC)) << std::endl;

  return 20 * log10(abs(S_MUSIC));
}

void beam_weight_float(RX_DOA_PARAMETER_2 *doa, float *bws)
{
  // generate the signal
  // time initial
  // float timeStart, timeEnd;
  // parameter setting
  const int fc = 180e+6;
  const int c = 3e+8;

  const float lemda = (double)c / (double)fc;
  std::complex<float> d(lemda * 0.5);
  std::complex<float> kc(2.0 * PI / lemda);
  int M = doa->antenna_num;
  const int nd = 14 * doa->ofdm_symbol_size;
  // angle setting
  const int len_t_theta = doa->angle_num;

  // printf("len_t_theta :%d\n",len_t_theta);
  std::complex<float> *t_theta = (std::complex<float> *)malloc(len_t_theta * sizeof(std::complex<float>));

  printf(YELLOW "test beam_weight_float Input angle:\t" CLOSE);
  for (int a = 0; a < len_t_theta; a++) {
    t_theta[a].real(doa->ptr_input_angle[a]);
    printf("%.0f, ", t_theta[a].real());
  }
  printf("\nlen_t_theta = %d\n", len_t_theta);
  printf("OFDM sumbol =%d\n", nd);
  printf("antenna_num M = %d\n", M);
  printf("\n--------------------------------------------\n");

#ifdef PRINT_RESULT
  std::cout << "Theta(degree):\t\t[";
  for (int i = 0; i < len_t_theta; ++i) {
    if (i != len_t_theta - 1)
      std::cout << t_theta[i].real() << ", ";
    else
      std::cout << t_theta[i].real() << "]\n\n";
  }
#endif

  // A_theta
  std::complex<double> *A_theta = (std::complex<double> *)malloc(M * len_t_theta * sizeof(std::complex<double>));
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < len_t_theta; j++) {
      // A_theta[i] = exp(I_1 * (t_theta[i].real()) * std::complex<double>(i));
      A_theta[i * len_t_theta + j] = exp(I_1 * kc * complex<float>(i + 1) * d * sin(t_theta[j] * complex<float>(PI / 180)));
      // std::cout << "A_theta[" << i * len_t_theta + j << "] = " << A_theta[i * len_t_theta + j] << " = exp(" << I_1 << kc << i << d << sin(t_theta[j] * complex<float>(PI / 180)) << std::endl;
    }
  }

  // print_complex_matrix(A_theta, 1 ,8);
  // std::cout << std::endl << "-----------------------------------------" << std::endl << std::endl;

  for (int i = 0; i < M; ++i) {
    // A_theta[i] = pow(2,14)* A_theta[i];

    A_theta[i] = pow(2, 6) * A_theta[i];
  }

  // print_complex_matrix(A_theta, 1 ,8);

  // int16_t *test_1 = (int16_t*)malloc( 2 * 8 * len_t_theta * sizeof(int16_t));
  // std::cout<<(int16_t)A_theta[0].real()<< std::endl;
  //  for(int i = 0; i < len_t_theta; ++i) {
  //      test_1[i*2] = (int16_t)A_theta[i].real();
  //      test_1[i*2+1] = (int16_t)A_theta[i].imag();
  //      std::cout << "[" << i << "] " << test_1[i * 2] << " " << test_1[i * 2 + 1] << std::endl;
  //  }

  for (int i = 0; i < M; ++i) {
    bws[i * 2] = (float)A_theta[i].real();
    bws[i * 2 + 1] = (float)A_theta[i].imag();
    // std::cout << "[" << i << "] " << test_1[i * 2] << " " << test_1[i * 2 + 1] << std::endl;
  }

  free(t_theta);
  free(A_theta);
}