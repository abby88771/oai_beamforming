// AVX512
// g++ -o cu_func cu_function.cpp -Wall -Wextra -std=c++14
// ./cu_func
#include <immintrin.h>
// C++
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <random>
#include <ccomplex>

// oai
// C
extern "C" {
#include <complex.h>
#include <assert.h>
// #include "PHY/defs_gNB.h"
#include <doa.h>
#include "color.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
}
//   std::cout << std::setprecision(6) << "Elapsed cmm:\t\t" << (timecomplex_matrix_end - timecomplex_matrix_start) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;
// #include "cu_function.h"

// #include "matplotlib-cpp/matplotlibcpp.h"

#define PI acos(-1)
// #define cudaCheck(ans) {cudaAssert((ans), __FILE__, __LINE__);}

#define BLOCK_SIZE 16
#define PRINT_RESULT 1
#define PLOT_RESULT 0

using namespace std::literals::complex_literals;
const std::complex<double> I_1(0, 1);
// extern "C" {
// __attribute__((aligned(64))) double mat_C[1000000] = {0.0};
// }
static float total_multiply_time = 0;
// using namespace std::complex_literals;
//  namespace plt = matplotlibcpp;

// inline void cudaAssert(cudaError_t code, const char *file, int line) {
//     if(code != cudaSuccess) {
//         fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
//         exit(code);
//     }
// }

// // warm up gpu for time measurement
// __global__ void warmup() {}

// print complex matrix matlab
void print_complex_matrix_matlab(std::complex<double> *matA, int rowA, int colA)
{
  std::cout << "[";
  for (int i = 0; i < rowA; ++i) {
    for (int j = 0; j < colA; ++j) {
      std::cout << std::setprecision(4) << matA[i * colA + j].real() << "+" << matA[i * colA + j].imag() << "i ";
    }
    std::cout << ";" << std::endl;
  }
  std::cout << "]" << std::endl;
}

// print complex matrix
void print_complex_matrix(std::complex<double> *matA, int rowA, int colA)
{
  for (int i = 0; i < rowA; ++i) {
    for (int j = 0; j < colA; ++j) {
      std::cout << std::fixed << std::setprecision(2) << std::setw(10) << matA[i * colA + j] << " ";
      // printf("\t%.0f ", matA[i * colA + j].real());
      // printf("+ %.0fi", matA[i * colA + j].imag());
      //  std::cout << std::setprecision(6) << matA[i * colA + j] << " ";
    }
    printf("\n");
  }
}

// generate random number with normal_distribution
std::complex<double> randn()
{
  std::random_device randomness_device{};
  std::mt19937 pseudorandom_generator{randomness_device()};
  auto mean = 0.0;
  auto std_dev = 1.0;
  std::normal_distribution<> distribution{mean, std_dev};
  auto sample = distribution(pseudorandom_generator);
  return (std::complex<double>)(sample);
}

std::complex<double> temp_real = {0, 1};
// add white gaussian noise
void awgn(std::complex<double> *input_signal, std::complex<double> *output_signal, int snr, int row, int col)
{
  std::complex<double> Esym;
  std::complex<double> No;
  std::complex<double> noiseSigma;
  std::complex<double> n;
  for (int i = 0; i < row * col; i++) {
    Esym += pow(abs(input_signal[i]), 2) / std::complex<double>(row * col);
    No = Esym / std::complex<double>(snr);
    noiseSigma = sqrt(No / std::complex<double>(2));
    n = noiseSigma * (randn() + randn() * temp_real);
    output_signal[i] = input_signal[i] + n;
    // std::cout << "---awgn output_signal---" << output_signal[i] << std::endl;
  }
}

// complex matrix addition
void complex_matrix_addition(std::complex<double> *matA, std::complex<double> *matB, int rowA, int colA)
{
  for (int i = 0; i < rowA; ++i) {
    for (int j = 0; j < colA; ++j) {
      matA[i * colA + j].real(matA[i * colA + j].real() + matB[i * colA + j].real());
      matA[i * colA + j].imag(matA[i * colA + j].imag() + matB[i * colA + j].imag());
    }
  }
}

// complex matrix subtraction
void complex_matrix_subtraction(std::complex<double> *matA, std::complex<double> *matB, int rowA, int colA)
{
  for (int i = 0; i < rowA; ++i) {
    for (int j = 0; j < colA; ++j) {
      matA[i * colA + j].real(matA[i * colA + j].real() - matB[i * colA + j].real());
      matA[i * colA + j].imag(matA[i * colA + j].imag() - matB[i * colA + j].imag());
    }
  }
}

struct timespec diff(struct timespec start, struct timespec end)
{
  struct timespec temp;
  if ((end.tv_nsec - start.tv_nsec) < 0) {
    temp.tv_sec = end.tv_sec - start.tv_sec - 1;
    temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
  } else {
    temp.tv_sec = end.tv_sec - start.tv_sec;
    temp.tv_nsec = end.tv_nsec - start.tv_nsec;
  }
  return temp;
}

// complex matrix multiplication ,colB = rowA  ; rowB = colA (M=32 ~= 9.4ms)
void complex_matrix_multiplication(std::complex<double> *matA, std::complex<double> *matB, std::complex<double> *matC, int rowA, int rowB, int colB)
{
  //----------------------------------------------------------
  struct timespec start, end;
  double time_used;
  //----------------------------------------------------------
  struct timeval start_scalar, end_scalar, diff_scalar;
  memset(matC, 0, rowA * colB * sizeof(std::complex<double>));
  __m256d simd_matA, simd_matB, simd_matC;

  clock_gettime(CLOCK_MONOTONIC, &start);
  gettimeofday(&start_scalar, NULL);
  for (int i = 0; i < rowA; ++i) {
    for (int j = 0; j < colB; ++j) {
      for (int k = 0; k < rowB; ++k) {
        matC[i * colB + j] += matA[i * rowB + k] * matB[k * colB + j];
      }
    }
  }
  clock_gettime(CLOCK_MONOTONIC, &end);
  gettimeofday(&end_scalar, NULL);
  timersub(&end_scalar, &start_scalar, &diff_scalar);
  total_multiply_time += diff_scalar.tv_usec; // sum of diff_multiply

  struct timespec temp = diff(start, end);
  time_used = temp.tv_sec + (double)temp.tv_nsec / 1000000000.0;
  // printf(L_PURPLE "-------------------------\nTime = %.0f(ns)\n" CLOSE, time_used * 1000000000);
  // printf(L_PURPLE "\nElapsed scalar time: %ld(us)\n" CLOSE, (long int)diff_scalar.tv_usec);
}

// get complex matrix by column
void complex_matrix_get_columns(std::complex<double> *matA, std::complex<double> *matCol, int rowA, int colA, int colTarget)
{
  for (int i = 0; i < rowA; ++i) {
    matCol[i] = matA[i * colA + colTarget];
  }
}

// get complex matrix by row
void complex_matrix_get_rows(std::complex<double> *matA, std::complex<double> *matRow, int rowA, int colA, int rowTarget)
{
  for (int i = 0; i < colA; ++i) {
    matRow[i] = matA[rowTarget * colA + i];
  }
}

// complex matrix conjugate transpose (M=32 -> 0.001ms)
void complex_matrix_conjugate_transpose(std::complex<double> *matA, int rowA, int colA)
{
  float cmct_start, cmct_end;
  // cmct_start = clock();
  std::complex<double> *temp = (std::complex<double> *)malloc(colA * rowA * sizeof(std::complex<double>));
  memcpy(temp, matA, (rowA * colA * sizeof(std::complex<double>)));
  for (int i = 0; i < rowA; ++i) {
    for (int j = 0; j < colA; ++j) {
      matA[j * rowA + i].real(temp[i * colA + j].real());
      matA[j * rowA + i].imag(-temp[i * colA + j].imag());
    }
  }
  // cmct_end = clock();
  // std::cout << std::setprecision(6) << CYAN "cmct:\t\t" << (cmct_end - cmct_start) / CLOCKS_PER_SEC * 1000 << " ms" CLOSE << std::endl;
  free(temp);
}

// complex matrix conjugate transpose and multiplication
void complex_matrix_conjugate_transpose_multiplication(std::complex<double> *matA, std::complex<double> *matB, int rowA, int colA)
{
  float cmctm_start, cmctm_end;
  std::complex<double> *temp = (std::complex<double> *)malloc(colA * rowA * sizeof(std::complex<double>));
  memcpy(temp, matA, (rowA * colA * sizeof(std::complex<double>)));
  // cmctm_start = clock();
  complex_matrix_conjugate_transpose(temp, rowA, colA);
  complex_matrix_multiplication(matA, temp, matB, rowA, colA, rowA); // colB = rowA
  // cmctm_end = clock();
  // std::cout << std::setprecision(6) << "Elapsed memcpy:\t\t" << (cmctm_end - cmctm_start) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;

  free(temp);
}

// compute Pn: matlab co.de: (Pn=Pn+vet_noise(:,ii)*vet_noise(:,ii)';), where (ii=1:length(vet_noise(1,:)))
void compute_Pn(std::complex<double> *Pn, std::complex<double> *vet_noise, int M, int len_t_theta)
{
  std::complex<double> *vet_noise_temp = (std::complex<double> *)malloc(M * sizeof(std::complex<double>));
  std::complex<double> *Pn_temp = (std::complex<double> *)malloc(M * M * sizeof(std::complex<double>));
  for (int i = 0; i < M - len_t_theta; ++i) {
    complex_matrix_get_columns(vet_noise, vet_noise_temp, M, M - len_t_theta, i);
    complex_matrix_conjugate_transpose_multiplication(vet_noise_temp, Pn_temp, M, 1);
    complex_matrix_addition(Pn, Pn_temp, M, M);
  }
  free(vet_noise_temp);
  free(Pn_temp);
}

// compute S_MUSIC: matlab code: (S_MUSIC(i)=1/(a_vector'*Pn*a_vector))
std::complex<double> compute_S_MUSIC(std::complex<double> *a_vector, std::complex<double> *Pn, int M)
{
  std::complex<double> *Pn_a_vector_temp = (std::complex<double> *)malloc(M * sizeof(std::complex<double>));
  std::complex<double> *S_MUSIC_temp = (std::complex<double> *)malloc(sizeof(std::complex<double>));
  complex_matrix_multiplication(Pn, a_vector, Pn_a_vector_temp, M, M, 1);
  complex_matrix_conjugate_transpose(a_vector, M, 1);
  complex_matrix_multiplication(a_vector, Pn_a_vector_temp, S_MUSIC_temp, 1, M, 1);
  std::complex<double> S_MUSIC = std::complex<double>(1) / S_MUSIC_temp[0];
  // std::cout << "---S_MUSIC---" << S_MUSIC << std::endl;
  // std::cout << "---std::complex<double>(1)---" << std::complex<double>(1) << std::endl;
  free(Pn_a_vector_temp);
  free(S_MUSIC_temp);
  return S_MUSIC;
}

// QR decomposer for c code
void qr(std::complex<double> *A, std::complex<double> *Q, std::complex<double> *R, int row, int col)
{
  std::complex<double> *Q_col = (std::complex<double> *)malloc(row * 1 * sizeof(std::complex<double>));
  std::complex<double> *vector_cur = (std::complex<double> *)malloc(row * 1 * sizeof(std::complex<double>));
  std::complex<double> *Qvector_cur = (std::complex<double> *)malloc(row * 1 * sizeof(std::complex<double>));
  std::complex<double> *power_cur = (std::complex<double> *)malloc(sizeof(std::complex<double>));
  std::complex<double> *power_val = (std::complex<double> *)malloc(sizeof(std::complex<double>));
  std::complex<double> *proj_val = (std::complex<double> *)malloc(sizeof(std::complex<double>));
  std::complex<double> *proj_Qvector_cur = (std::complex<double> *)malloc(row * 1 * sizeof(std::complex<double>));
  for (int i = 0; i < row * col; i += (col + 1)) {
    Q[i].real(1); // value 1 (unit matrix)
    R[i].real(1); // value 1 (unit matrix)
  }
  for (int i = 0; i < col; ++i) {
    for (int m = 0; m < row; ++m) {
      Q[m * col + i] = A[m * col + i];
    }
    complex_matrix_get_columns(Q, Q_col, row, col, i);
    // conjuate Q_col
    complex_matrix_conjugate_transpose(Q_col, row, 1);
    memset(power_cur, 0, sizeof(std::complex<double>));
    complex_matrix_conjugate_transpose_multiplication(Q_col, power_cur, 1, row);
    power_cur[0] = sqrt(power_cur[0]);
    if (i > 0) {
      complex_matrix_get_columns(A, vector_cur, row, col, i);
      std::complex<double> *Q_col_proj = (std::complex<double> *)malloc(row * i * sizeof(std::complex<double>));
      std::complex<double> *proj_vector = (std::complex<double> *)malloc(i * sizeof(std::complex<double>));
      memset(proj_vector, 0, i * sizeof(std::complex<double>));
      for (int j = 0; j < i; ++j) {
        for (int m = 0; m < row; ++m) {
          Q_col_proj[m * i + j] = Q[m * col + j];
        }
      }
      complex_matrix_conjugate_transpose(Q_col_proj, row, i);
      complex_matrix_multiplication(Q_col_proj, vector_cur, proj_vector, i, row, 1);
      complex_matrix_conjugate_transpose(Q_col_proj, i, row);
      memset(Q_col, 0, row * 1 * sizeof(std::complex<double>));
      complex_matrix_multiplication(Q_col_proj, proj_vector, Q_col, row, i, 1);
      complex_matrix_subtraction(vector_cur, Q_col, row, 1);
      for (int m = 0; m < row; ++m) {
        Q[m * col + i] = vector_cur[m];
      }
      for (int j = 0; j < i; ++j) {
        R[i + col * j] = proj_vector[j];
      }
      free(Q_col_proj);
      free(proj_vector);
    }
    complex_matrix_get_columns(Q, Q_col, row, col, i);
    // conjuate Q_col
    complex_matrix_conjugate_transpose(Q_col, row, 1);
    memset(power_val, 0, sizeof(std::complex<double>));
    complex_matrix_conjugate_transpose_multiplication(Q_col, power_val, 1, row);
    power_val[0] = sqrt(power_val[0]);

    // 1e-4 = 0.0001
    if (power_val[0].real() / power_cur[0].real() < 1e-4) {
      R[i * row + i] = 0;
      // span again
      for (int m = 0; m < row; ++m) {
        Q[m * col + i] = 0;
      }
      Q[i * row + i].real(1);
      complex_matrix_get_columns(Q, vector_cur, row, col, i);
      for (int j = 0; j < i; ++j) {
        complex_matrix_get_columns(Q, Qvector_cur, row, col, j);
        memset(proj_val, 0, sizeof(std::complex<double>));
        complex_matrix_conjugate_transpose(Qvector_cur, row, 1);
        complex_matrix_multiplication(Qvector_cur, vector_cur, proj_val, 1, row, 1);
        complex_matrix_conjugate_transpose(Qvector_cur, 1, row);
        complex_matrix_get_columns(Q, Q_col, row, col, i);
        memset(proj_Qvector_cur, 0, row * 1 * sizeof(std::complex<double>));
        complex_matrix_multiplication(Qvector_cur, proj_val, proj_Qvector_cur, row, 1, 1);
        complex_matrix_subtraction(Q_col, proj_Qvector_cur, row, 1);
        for (int m = 0; m < row; ++m) {
          Q[m * col + i] = Q_col[m];
        }
      }
      complex_matrix_get_columns(Q, Q_col, row, col, i);
      complex_matrix_conjugate_transpose(Q_col, row, 1);
      memset(power_val, 0, sizeof(std::complex<double>));
      complex_matrix_conjugate_transpose_multiplication(Q_col, power_val, 1, row);
      power_val[0] = sqrt(power_val[0]);
      complex_matrix_conjugate_transpose(Q_col, 1, row);
      for (int m = 0; m < row; ++m) {
        Q[m * col + i] /= power_val[0]; // Q[m * col + i] = Q[m * col + i] / power_val[0]
      }
    } else {
      R[i * row + i] = power_val[0];
      for (int m = 0; m < row; ++m) {
        Q[m * col + i] /= power_val[0];
      }
    }
  }
  free(Q_col);
  free(vector_cur);
  free(Qvector_cur);
  free(power_cur);
  free(power_val);
  free(proj_val);
  free(proj_Qvector_cur);
}

// compute eigen upper triangular
void eigen_upper_triangular(std::complex<double> *A, std::complex<double> *eigenvalue, std::complex<double> *eigenvector, int row, int col)
{
  std::complex<double> *vector_cur = (std::complex<double> *)malloc(row * 1 * sizeof(std::complex<double>));
  std::complex<double> *eigen_element_cur = (std::complex<double> *)malloc(sizeof(std::complex<double>));
  std::complex<double> *vector_cur_temp = (std::complex<double> *)malloc(sizeof(std::complex<double>));
  std::complex<double> *A_col = (std::complex<double> *)malloc(1 * col * sizeof(std::complex<double>));
  std::complex<double> diff_eigen_value = 0;
  for (int i = 0; i < row; ++i) {
    for (int j = 0; j < col; ++j) {
      if (i > j) {
        A[i * col + j].real(0);
        A[i * col + j].imag(0);
      }
      if (i == j) {
        eigenvalue[i * col + j] = A[i * col + j];
        eigenvector[i * col + j].real(1);
        // printf(PURPLE "eigenvalue[%d] = %.2f\n" CLOSE, i * col + j, eigenvalue[i * col + j]);
      }
    }
  }
  for (int i = 0; i < col; ++i) {
    complex_matrix_get_columns(eigenvector, vector_cur, row, col, i);
    for (int j = i - 1; j > -1; --j) {
      diff_eigen_value = eigenvalue[i * col + i] - eigenvalue[j * col + j];
      if (diff_eigen_value.real() < 1e-8)
        eigen_element_cur[0] = 0;
      else {
        complex_matrix_get_rows(A, A_col, row, col, j);
        complex_matrix_multiplication(A_col, vector_cur, eigen_element_cur, 1, row, 1);
        eigen_element_cur[0] = eigen_element_cur[0] / diff_eigen_value;
      }
      vector_cur[j] = eigen_element_cur[0];
    }
    complex_matrix_conjugate_transpose(vector_cur, row, 1);
    complex_matrix_conjugate_transpose_multiplication(vector_cur, vector_cur_temp, 1, row);
    vector_cur_temp[0] = sqrt(vector_cur_temp[0]);
    complex_matrix_conjugate_transpose(vector_cur, 1, row);
    for (int m = 0; m < row; ++m) {
      eigenvector[m * col + i] = vector_cur[m] / vector_cur_temp[0];
    }
  }
  free(vector_cur);
  free(eigen_element_cur);
  free(vector_cur_temp);
  free(A_col);
}

// compute complex eigenvector and eigenvalue for c code
// void eigen(A:input array, Ve:eigen vector, De:eigen value, row , col , iter)
void eigen(std::complex<double> *A, std::complex<double> *Ve, std::complex<double> *De, int row, int col, int iter)
{
  float time_QR_start, time_QR_end; // time initial
  std::complex<double> *Q = (std::complex<double> *)calloc(row * col, sizeof(std::complex<double>));
  std::complex<double> *R = (std::complex<double> *)calloc(row * col, sizeof(std::complex<double>));
  std::complex<double> *Q_temp = (std::complex<double> *)calloc(row * col, sizeof(std::complex<double>));
  std::complex<double> *Q_temp_clone = (std::complex<double> *)calloc(row * col, sizeof(std::complex<double>));
  for (int i = 0; i < row * col; i += (col + 1)) {
    Q_temp[i].real(1);
  }
  time_QR_start = clock();
  for (int i = 0; i < iter; ++i) {
    qr(A, Q, R, row, col);
    complex_matrix_multiplication(R, Q, A, row, row, col);
    complex_matrix_multiplication(Q_temp, Q, Q_temp_clone, row, row, col);
    memcpy(Q_temp, Q_temp_clone, row * col * sizeof(std::complex<double>));
  }
  time_QR_end = clock();
  std::cout << std::setprecision(6) << L_CYAN "Elapsed QR:\t\t" << (time_QR_end - time_QR_start) / CLOCKS_PER_SEC * 1000 << " ms, Iteration = " << iter << CLOSE << std::endl;

  for (int i = 0; i < row; ++i) {
    for (int j = 0; j < col; ++j) {
      if (i > j)
        A[i * col + j] = 0;
    }
  }
  std::complex<double> *YY0 = (std::complex<double> *)calloc(row * col, sizeof(std::complex<double>));
  std::complex<double> *XX0 = (std::complex<double> *)calloc(row * col, sizeof(std::complex<double>));
  eigen_upper_triangular(A, YY0, XX0, row, col);
  memcpy(De, YY0, row * col * sizeof(std::complex<double>));
  complex_matrix_multiplication(Q_temp, XX0, Ve, row, row, col);
  free(Q);
  free(R);
  free(Q_temp);
  free(Q_temp_clone);
  free(YY0);
  free(XX0);
}

// compute the MUSIC DOA in one dimension on CPU
void MUSIC_DOA_1D_CPU(int M, int snr, int qr_iter, int angle, float *result)
{
#ifdef PRINT_RESULT
  printf("--Parameter--\n");
  printf("Antenna count:\t\t%d\n", M);
  printf("SNR:\t\t\t%d\n", snr);
  printf("QR iteration:\t\t%d\n", qr_iter);
  printf("Angle:\t\t%d\n", angle);
// printf("Multiple input size:\t%d\n", multi_input);
#endif
  // generate the signal
  // time initial
  float timeStart, timeEnd;
  // parameter setting
  const int fc = 180e+6;
  const int c = 3e+8;
  const double lemda = (double)c / (double)fc;
  std::complex<double> d(lemda * 0.5);
  std::complex<double> kc(2.0 * PI / lemda);
  const int nd = 500;
  // angle setting
  const int len_t_theta = 1;
  std::complex<double> *t_theta = (std::complex<double> *)malloc(len_t_theta * sizeof(std::complex<double>));
  t_theta[0].real(angle);
// t_theta[1].real(12);
// t_theta[2].real(20);
#ifdef PRINT_RESULT
  std::cout << "Theta(degree):\t\t[";
  for (int i = 0; i < len_t_theta; ++i) {
    if (i != len_t_theta - 1)
      std::cout << t_theta[i].real() << ", ";
    else
      std::cout << t_theta[i].real() << "]\n\n";
  }
  std::cout << "---Time---" << std::endl;
#endif
  // A_theta matrix (M, length of t_theta)
  std::complex<double> *A_theta = (std::complex<double> *)malloc(M * len_t_theta * sizeof(std::complex<double>));
  for (int i = 0; i < M; ++i) {
    for (int j = 0; j < len_t_theta; ++j) {
      A_theta[i * len_t_theta + j] = exp(I_1 * kc * std::complex<double>(i) * d * sin(t_theta[j] * std::complex<double>(PI / 180)));
    }
  }
  // t_sig matrix (length of t_theta, nd)
  std::complex<double> *t_sig = (std::complex<double> *)malloc(len_t_theta * nd * sizeof(std::complex<double>));
  for (int i = 0; i < len_t_theta; ++i) {
    for (int j = 0; j < nd; ++j) {
      t_sig[i * nd + j] = (randn() + randn() * I_1) / std::complex<double>(sqrt(2));
      // if(i == 0) t_sig[i * nd + j] *= (std::complex<double>)2;
    }
  }
  // sig_co matrix (M, nd)
  std::complex<double> *sig_co = (std::complex<double> *)malloc(M * nd * sizeof(std::complex<double>));
  // compute sig_co
  complex_matrix_multiplication(A_theta, t_sig, sig_co, M, len_t_theta, nd);

  // receiver
  // x_r matrix (M, nd)
  std::complex<double> *x_r = (std::complex<double> *)malloc(M * nd * sizeof(std::complex<double>));
  // memcpy(x_r, sig_co, M * nd * sizeof(std::complex<double>));
  // add noise to the signal
  awgn(sig_co, x_r, snr, M, nd);

  // music algorithm
  // R_xx matrix (M, M)
  std::complex<double> *R_xx = (std::complex<double> *)malloc(M * M * sizeof(std::complex<double>));
  // matlab code:  (R_xx = 1 / M * x_r * x_r')
  complex_matrix_conjugate_transpose_multiplication(x_r, R_xx, M, nd);
  for (int i = 0; i < M * M; ++i)
    R_xx[i] /= std::complex<double>(M);
  // compute eigenvector Ve (M, M)
  std::complex<double> *Ve = (std::complex<double> *)malloc(M * M * sizeof(std::complex<double>));
  std::complex<double> *De = (std::complex<double> *)malloc(M * M * sizeof(std::complex<double>));
  // timestamp start
  timeStart = clock();
  // eigen(R_xx, Ve, De, M, M, qr_iter);
  // printf("Ve \n");
  // print_complex_matrix(Ve,M,M);
  // printf("De \n");
  // print_complex_matrix(De,M,M);
  // return;
  // get vet_noise (M, M - len_t_theta): part of Ve (eigenvector)
  std::complex<double> *vet_noise = (std::complex<double> *)malloc(M * (M - len_t_theta) * sizeof(std::complex<double>));
  for (int i = 0; i < M; ++i) {
    for (int j = len_t_theta; j < M; ++j) {
      vet_noise[i * (M - len_t_theta) + j - len_t_theta] = Ve[i * M + j];
    }
  }
  // Pn matrix (M, M)
  std::complex<double> *Pn = (std::complex<double> *)calloc(M * M, sizeof(std::complex<double>));
  compute_Pn(Pn, vet_noise, M, len_t_theta);
  // timestamp end
  timeEnd = clock();
#ifdef PRINT_RESULT
  std::cout << std::setprecision(6) << "MUSIC (cpu):\t\t" << (timeEnd - timeStart) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;
#endif
  result[2] += (timeEnd - timeStart) / CLOCKS_PER_SEC * 1000;

  // array pattern
  // parameter setting
  const int len_dth = 401;
  double *dth = (double *)malloc(len_dth * sizeof(double));
  double *dr = (double *)malloc(len_dth * sizeof(double));
  for (int i = 0; i < len_dth; ++i) { // do only one time, no need to be paralleled
    dth[i] = -10 + 0.1 * i;
    dr[i] = dth[i] * PI / 180;
  }
  // compute S_MUSIC_dB
  std::complex<double> *a_vector = (std::complex<double> *)malloc(M * sizeof(std::complex<double>));
  std::complex<double> *S_MUSIC = (std::complex<double> *)malloc(len_dth * sizeof(std::complex<double>));
  double *S_MUSIC_dB = (double *)malloc(len_dth * sizeof(double));
  // timestamp start
  timeStart = clock();
  for (int i = 0; i < len_dth; ++i) { // can be paralleled to compute S_MUSIC_dB
    for (int j = 0; j < M; ++j) {
      a_vector[j] = exp(I_1 * kc * (std::complex<double>)j * d * sin(dr[i]));
    }
    S_MUSIC[i] = compute_S_MUSIC(a_vector, Pn, M);
    // compute S_MUSIC_dB
    S_MUSIC_dB[i] = 20 * log10(abs(S_MUSIC[i]));
  }
  // find Max and position
  double max_temp = S_MUSIC_dB[0];
  int position = 0;
  for (int i = 0; i < len_dth; ++i) {
    if (S_MUSIC_dB[i] > max_temp) {
      max_temp = S_MUSIC_dB[i];
      position = i;
    }
  }
  // timestamp end
  timeEnd = clock();
#ifdef PRINT_RESULT
  std::cout << std::setprecision(6) << "Array pattern (cpu):\t" << (timeEnd - timeStart) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;
  // print the result
  printf("\n--Result--\n");
  std::cout << "Theta estimation:\t" << dth[position] << std::endl;
#endif
  float error[3];
  float errorFinal = 0;
  for (int i = 0; i < 3; ++i) {
    error[i] = abs(dth[position] - t_theta[i].real());
    if (i == 0)
      errorFinal = error[i];
    else if (error[i] < errorFinal)
      errorFinal = error[i];
  }
  if (errorFinal > result[0])
    result[0] = errorFinal;
  else if (errorFinal == 0)
    result[1]++;

  // plot the result
  // #ifdef PLOT_RESULT
  // std::vector<double> S_MUSIC_dB_vec(S_MUSIC_dB, S_MUSIC_dB + len_dth);
  // std::vector<double> dth_vec(dth, dth + len_dth);
  // plt::plot(dth_vec, S_MUSIC_dB_vec, "blue");
  // plt::title("MUSIC DOA Estimation");
  // plt::xlabel("Theta (degree)");
  // plt::ylabel("Power Spectrum (dB)");
  // plt::xlim(dth[0], dth[len_dth - 1]);
  // plt::grid(true);
  // plt::show();
  // #endif

  // free memory
  free(t_theta);
  free(A_theta);
  free(t_sig);
  free(sig_co);
  free(x_r);
  free(R_xx);
  free(Ve);
  free(De);
  free(vet_noise);
  free(Pn);
  free(dth);
  free(dr);
  free(a_vector);
  free(S_MUSIC);
  free(S_MUSIC_dB);
}

void MUSIC_DOA_1D_CPU_test(int M, int qr_iter, int angle, float *result, int SNR)
{
  // time initial
  float timeMusic_start, timeMusic_end; // Total MUSIC Algorithm time
  float timeAwgn_start, timeAwgn_end; // AWGN white noise time
  float timecomplex_matrix_start, timecomplex_matrix_end; // complex_matrix time
  float timeconjugate_matrix_start, timeconjugate_matrix_end; // complex_matrix_conjugate_transpose_multiplication time
  float timeStart, timeEnd; // (cpu)time
  float timeEigen_start, timeEigen_end; // Eigen function time
  float timeCompute_start, timeCompute_end; // Compute_Pn time
  struct timeval start_cmctm, end_cmctm, diff_cmctm;
  timeMusic_start = clock();
#ifdef PRINT_RESULT
  printf("---------------\n");
  printf("--MUSIC DOA--\n");
  printf("---------------\n");
  printf("--Parameter--\n");
  printf("Antenna count:\t\t%d\n", M);
  printf("SNR:\t\t\t%d\n", SNR);
  printf("QR iteration:\t\t%d\n", qr_iter);

#endif
  // generate the signal
  // float timeStart_1, timeEnd_1;
  //  parameter setting
  const int fc = 180e+6;
  const int c = 3e+8;
  const double lemda = (double)c / (double)fc;
  std::complex<double> d(lemda * 0.5); // For Real part
  std::complex<double> kc(2.0 * PI / lemda); // For Real part
  const int nd = 512; //
  // angle setting
  const int len_t_theta = 1;
  std::complex<double> *t_theta = (std::complex<double> *)malloc(len_t_theta * sizeof(std::complex<double>));
  t_theta[0].real(angle);
// t_theta[1].real(12);
// t_theta[2].real(20);
// timeStart_1 = clock();
#ifdef PRINT_RESULT
  std::cout << "Theta(degree):\t\t[";
  for (int i = 0; i < len_t_theta; ++i) {
    if (i != len_t_theta - 1)
      std::cout << t_theta[i].real() << ", ";
    else
      std::cout << t_theta[i].real() << "]\n\n";
  }
  std::cout << "---Time---" << std::endl;
#endif
  // A_theta matrix (M, length of t_theta)
  std::complex<double> *A_theta = (std::complex<double> *)malloc(M * len_t_theta * sizeof(std::complex<double>));
  for (int i = 0; i < M; ++i) {
    for (int j = 0; j < len_t_theta; ++j) {
      A_theta[i * len_t_theta + j] = exp(I_1 * kc * std::complex<double>(i) * d * sin(t_theta[j] * std::complex<double>(PI / 180)));
      // std::cout << "---A_theta---" << A_theta[i * len_t_theta + j] << std::endl;
    }
  }
  // t_sig matrix (length of t_theta, nd)
  std::complex<double> *t_sig = (std::complex<double> *)malloc(len_t_theta * nd * sizeof(std::complex<double>));
  // memcpy(t_sig, double_IQ, 2 * nd * sizeof(double));
  for (int i = 0; i < len_t_theta; ++i) {
    for (int j = 0; j < nd; ++j) {
      t_sig[i * nd + j] = (randn() + randn() * I_1) / std::complex<double>(sqrt(2));
      // std::cout << "---t_sig---" << t_sig[i * nd + j] << std::endl;

      // if(i == 0) t_sig[i * nd + j] *= (std::complex<double>)2;
    }
  }
  timecomplex_matrix_start = clock();
  // print_complex_matrix(t_sig ,1 ,nd);

  // sig_co matrix (M, nd)
  // timeEnd_1 = clock();
  std::complex<double> *sig_co = (std::complex<double> *)malloc(M * nd * sizeof(std::complex<double>));
  // compute sig_co
  complex_matrix_multiplication(A_theta, t_sig, sig_co, M, len_t_theta, nd);
  // print_complex_matrix(sig_co, M, nd);
  timecomplex_matrix_end = clock();

  // receiver
  // x_r matrix (M, nd)
  std::complex<double> *x_r = (std::complex<double> *)malloc(M * nd * sizeof(std::complex<double>));
  // memcpy(x_r, sig_co, M * nd * sizeof(std::complex<double>));
  // add noise to the signal
  timeAwgn_start = clock();
  awgn(sig_co, x_r, SNR, M, nd);
  // for (int a = 0; a < M * nd; a++)
  // {
  //     std::cout << "---awgn---" << x_r[a] << std::endl;
  // }

  timeAwgn_end = clock();
  // print_complex_matrix(x_r, M, nd );
  std::cout << std::setprecision(6) << "Elapsed AWGN:\t\t" << (timeAwgn_end - timeAwgn_start) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;

  // music algorithm
  // R_xx matrix (M, M)

  std::complex<double> *R_xx = (std::complex<double> *)malloc(M * M * sizeof(std::complex<double>));
  // matlab code:  (R_xx = 1 / M * x_r * x_r')
  // x_r = matA; R_xx = matB;  M = rowA;  nd = colA;
  timeconjugate_matrix_start = clock();
  gettimeofday(&start_cmctm, NULL);
  printf(GREEN "rowA = %d, colA = %d\n" CLOSE, M, nd);
  complex_matrix_conjugate_transpose_multiplication(x_r, R_xx, M, nd);
  // print_complex_matrix(R_xx, M, nd);

  gettimeofday(&end_cmctm, NULL);
  timeconjugate_matrix_end = clock();

  timersub(&end_cmctm, &start_cmctm, &diff_cmctm);
  std::cout << std::setprecision(6) << "Elapsed cmctm:\t\t" << (timeconjugate_matrix_end - timeconjugate_matrix_start) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;
  printf("\ncmctm time: %ld(usec)\n", (long int)diff_cmctm.tv_usec);

  for (int i = 0; i < M * M; ++i) {
    R_xx[i] /= std::complex<double>(M);
  }
  // compute eigenvector Ve (M, M)
  std::complex<double> *Ve = (std::complex<double> *)malloc(M * M * sizeof(std::complex<double>));
  std::complex<double> *De = (std::complex<double> *)malloc(M * M * sizeof(std::complex<double>));
  // timestamp start
  timeStart = clock(); // Total time

  timeEigen_start = clock(); // start Eigen time
  eigen(R_xx, Ve, De, M, M, qr_iter);
  // printf("----------Ve------------\n");
  // print_complex_matrix(Ve, M, M);
  // printf("----------De------------\n");
  // print_complex_matrix(De, M, M);
  // get vet_noise (M, M - len_t_theta): part of Ve (eigenvector)
  timeEigen_end = clock();
  std::cout << std::setprecision(6) << "Elapsed Eigen time:\t" << (timeEigen_end - timeEigen_start) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;

  std::complex<double> *vet_noise = (std::complex<double> *)malloc(M * (M - len_t_theta) * sizeof(std::complex<double>));
  for (int i = 0; i < M; ++i) {
    for (int j = len_t_theta; j < M; ++j) {
      vet_noise[i * (M - len_t_theta) + j - len_t_theta] = Ve[i * M + j];
      // std::cout << "--- vet_noise[i]---" << vet_noise[i * (M - len_t_theta) + j - len_t_theta] << std::endl;
    }
  }
  timeCompute_start = clock();
  // Pn matrix (M, M)
  std::complex<double> *Pn = (std::complex<double> *)calloc(M * M, sizeof(std::complex<double>));
  compute_Pn(Pn, vet_noise, M, len_t_theta);
  // printf("----------Pn------------\n");
  // print_complex_matrix(Pn, M, M);
  // timestamp end
  timeCompute_end = clock();
  std::cout << std::setprecision(6) << "Elapsed compute_Pn:\t" << (timeCompute_end - timeCompute_start) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;

  timeEnd = clock();
#ifdef PRINT_RESULT
  std::cout << std::setprecision(6) << "MUSIC (cpu):\t\t" << (timeEnd - timeStart) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;
// std::cout << std::setprecision(6) << "test: \t\t" << (timeEnd_1 - timeStart_1) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;
#endif
  result[2] += (timeEnd - timeStart) / CLOCKS_PER_SEC * 1000;

  // array pattern
  // parameter setting
  // const int len_dth = 401;
  const int len_dth = 1201;
  double *dth = (double *)malloc(len_dth * sizeof(double));
  double *dr = (double *)malloc(len_dth * sizeof(double));
  for (int i = 0; i < len_dth; ++i) { // do only one time, no need to be paralleled
    dth[i] = -60 + 0.1 * i;
    dr[i] = dth[i] * PI / 180;
  }
  // compute S_MUSIC_dB
  std::complex<double> *a_vector = (std::complex<double> *)malloc(M * sizeof(std::complex<double>));
  std::complex<double> *S_MUSIC = (std::complex<double> *)malloc(len_dth * sizeof(std::complex<double>));
  double *S_MUSIC_dB = (double *)malloc(len_dth * sizeof(double));

  // timestamp start
  timeStart = clock();
  for (int i = 0; i < len_dth; ++i) { // can be paralleled to compute S_MUSIC_dB
    for (int j = 0; j < M; ++j) {
      a_vector[j] = exp(I_1 * kc * (std::complex<double>)j * d * sin(dr[i]));
      // std::cout << "---a_vector---" << a_vector[j] << std::endl;
    }
    S_MUSIC[i] = compute_S_MUSIC(a_vector, Pn, M);
    // printf("\tS_MUSIC(%f,%f), ", S_MUSIC[i].real(), S_MUSIC[i].imag());

    // compute S_MUSIC_dB
    S_MUSIC_dB[i] = 20 * log10(abs(S_MUSIC[i]));
    // std::cout << "20log" << S_MUSIC[i] << " = " << 20 * log10(abs(S_MUSIC[i])) << std::endl;
    // printf("S_MUSIC_dB[%d] = %.4f\n", i, S_MUSIC_dB[i]);
  }
  // find Max and position
  double max_temp = S_MUSIC_dB[0];

  int position = 0;
  for (int i = 0; i < len_dth; ++i) {
    if (S_MUSIC_dB[i] > max_temp) {
      max_temp = S_MUSIC_dB[i];
      position = i;
      // printf(YELLOW "max_temp = %f(dB)\n" CLOSE, S_MUSIC_dB[i]);
    }
  }

  printf("position : %d\n", position);
  // timestamp end
  timeEnd = clock();
#ifdef PRINT_RESULT
  std::cout << std::setprecision(6) << "Array pattern (cpu):\t" << (timeEnd - timeStart) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;
  printf("\n--Result--\n");
  std::cout << "Theta estimation:\t" << dth[position] << std::endl;

  timeMusic_end = clock(); // Total time
  // std::cout << std::setprecision(6) << L_GREEN "Total MUSIC time:\t" << (timeMusic_end - timeMusic_start) / CLOCKS_PER_SEC * 1000 << " ms" CLOSE << std::endl;
  // std::cout << std::setprecision(6) << L_GREEN "Total Multiply time:\t" << total_multiply_time / 1000 << " ms" CLOSE << std::endl
  //    << "-----------------------------------------" << std::endl
  //    << std::endl;
#endif
  float error;
  error = abs(dth[position] - t_theta[0].real());

  if (error > result[0])
    result[0] = error;
  if (error != 0)
    result[1] += pow(error, 2);

    // plot the result
#ifdef PLOT_RESULT
// std::vector<double> S_MUSIC_dB_vec(S_MUSIC_dB, S_MUSIC_dB + len_dth);
// std::vector<double> dth_vec(dth, dth + len_dth);
// plt::plot(dth_vec, S_MUSIC_dB_vec, "blue");
// plt::title("MUSIC DOA Estimation");
// plt::xlabel("Theta (degree)");
// plt::ylabel("Power Spectrum (dB)");
// plt::xlim(dth[0], dth[len_dth - 1]);
// plt::grid(true);
// plt::show();
#endif

  // free memory
  free(t_theta);
  free(A_theta);
  free(t_sig);
  free(sig_co);
  free(x_r);
  free(R_xx);
  free(Ve);
  free(De);
  free(vet_noise);
  free(Pn);
  free(dth);
  free(dr);
  free(a_vector);
  free(S_MUSIC);
  free(S_MUSIC_dB);
}

void MVDR_DOA_1D_CPU_test(int M, int qr_iter, int angle, float *result, int snr)
{
#ifdef PRINT_RESULT
  printf("--MVDR DOA--\n");
  printf("--Parameter--\n");
  printf("Antenna count:\t\t%d\n", M);
  printf("SNR:\t\t\t%d\n", snr);
  printf("QR iteration:\t\t%d\n", qr_iter);
  printf("Angle:\t\t\t%d\n", angle);
#endif
  // generate the signal
  // time initial
  float timeStart, timeEnd;
  // float timeStart_1, timeEnd_1;
  //  parameter setting
  const int fc = 180e+6;
  const int c = 3e+8;
  const double lemda = (double)c / (double)fc;
  std::complex<double> d(lemda * 0.5);
  std::complex<double> kc(2.0 * PI / lemda);
  const int nd = 500;
  // angle setting
  const int len_t_theta = 1;
  std::complex<double> *t_theta = (std::complex<double> *)malloc(len_t_theta * sizeof(std::complex<double>));
  t_theta[0].real(angle);
// t_theta[1].real(12);
// t_theta[2].real(20);
// timeStart_1 = clock();
#ifdef PRINT_RESULT
  std::cout << "Theta(degree):\t\t[";
  for (int i = 0; i < len_t_theta; ++i) {
    if (i != len_t_theta - 1)
      std::cout << t_theta[i].real() << ", ";
    else
      std::cout << t_theta[i].real() << "]\n\n";
  }
  std::cout << "---Time---" << std::endl;
#endif
  // A_theta matrix (M, length of t_theta)
  std::complex<double> *A_theta = (std::complex<double> *)malloc(M * len_t_theta * sizeof(std::complex<double>));
  for (int i = 0; i < M; ++i) {
    for (int j = 0; j < len_t_theta; ++j) {
      A_theta[i * len_t_theta + j] = exp(I_1 * kc * std::complex<double>(i) * d * sin(t_theta[j] * std::complex<double>(PI / 180)));
    }
  }
  // t_sig matrix (length of t_theta, nd)
  std::complex<double> *t_sig = (std::complex<double> *)malloc(len_t_theta * nd * sizeof(std::complex<double>));
  // memcpy(t_sig, double_IQ, 2 * nd * sizeof(double));
  for (int i = 0; i < len_t_theta; ++i) {
    for (int j = 0; j < nd; ++j) {
      // t_sig[i * nd + j] = (randn() + randn() * I_1) / std::complex<double>(sqrt(2));
      t_sig[i * nd + j] = (randn() + randn()) / std::complex<double>(sqrt(2));
      // if(i == 0) t_sig[i * nd + j] *= (std::complex<double>)2;
    }
  }
  // sig_co matrix (M, nd)
  std::complex<double> *sig_co = (std::complex<double> *)malloc(M * nd * sizeof(std::complex<double>));
  // compute sig_co
  complex_matrix_multiplication(A_theta, t_sig, sig_co, M, len_t_theta, nd);
  // receiver
  // x_r matrix (M, nd)
  std::complex<double> *x_r = (std::complex<double> *)malloc(M * nd * sizeof(std::complex<double>));
  // memcpy(x_r, sig_co, M * nd * sizeof(std::complex<double>));
  // add noise to the signal
  awgn(sig_co, x_r, snr, M, nd);

  // mvdr algorithm
  // R_xx matrix (M, M)
  std::complex<double> *R_xx = (std::complex<double> *)malloc(M * M * sizeof(std::complex<double>));
  // matlab code:  (R_xx = 1 / M * x_r * x_r')
  complex_matrix_conjugate_transpose_multiplication(x_r, R_xx, M, nd);
  // printf("----------R_xx------------\n");
  // print_complex_matrix(R_xx,M,M);
  for (int i = 0; i < M * M; ++i)
    R_xx[i] /= std::complex<double>(M);
  // printf("----------R_xx------------\n");
  // print_complex_matrix(R_xx,M,M);
  // compute eigenvector Ve (M, M)
  std::complex<double> *Ve = (std::complex<double> *)malloc(M * M * sizeof(std::complex<double>));
  std::complex<double> *De = (std::complex<double> *)malloc(M * M * sizeof(std::complex<double>));
  // timestamp start
  timeStart = clock();
  eigen(R_xx, Ve, De, M, M, qr_iter);
  // printf("----------Ve------------\n");
  // print_complex_matrix(Ve, M, M);
  // printf("----------De------------\n");
  // print_complex_matrix(De, M, M);
  std::complex<double> *R_xx_inv_1 = (std::complex<double> *)malloc(M * M * sizeof(std::complex<double>));
  std::complex<double> *Pn = (std::complex<double> *)calloc(M * M, sizeof(std::complex<double>));
  for (int i = 0; i < M * M; i += (M + 1)) {
    // std::cout << "abs(De)"<< abs(De[i]) << std::endl;

    if (abs(De[i]) < 0.00000000001) {
      De[i].real(1000000);
      De[i].imag(0);
    } else
      De[i] = std::complex<double>(1) / De[i];
  }
  // printf("----------R_xx------------\n");
  // print_complex_matrix(R_xx,M,M);
  complex_matrix_multiplication(Ve, De, R_xx_inv_1, M, M, M);
  // printf("----------R_xx_inv_1------------\n");
  // print_complex_matrix(R_xx_inv_1,M,M);
  complex_matrix_conjugate_transpose(Ve, M, M);
  complex_matrix_multiplication(R_xx_inv_1, Ve, Pn, M, M, M);

  // printf("----------Pn------------\n");
  // print_complex_matrix(Pn, M, M);
  timeEnd = clock();
#ifdef PRINT_RESULT
  std::cout << std::setprecision(6) << "MVDR (cpu):\t\t" << (timeEnd - timeStart) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;
#endif
  result[2] += (timeEnd - timeStart) / CLOCKS_PER_SEC * 1000;

  // array pattern
  // parameter setting
  // const int len_dth = 401;
  const int len_dth = 1201;
  double *dth = (double *)malloc(len_dth * sizeof(double));
  double *dr = (double *)malloc(len_dth * sizeof(double));
  for (int i = 0; i < len_dth; ++i) { // do only one time, no need to be paralleled
    // dth[i] = -10 + 0.1 * i;
    dth[i] = -60 + 0.1 * i;
    dr[i] = dth[i] * PI / 180;
    // printf("dth[%d] : %f\n",i,dth[i]);
  }
  // compute S_MVDR_dB
  std::complex<double> *a_vector = (std::complex<double> *)malloc(M * sizeof(std::complex<double>));
  std::complex<double> *S_MVDR = (std::complex<double> *)malloc(len_dth * sizeof(std::complex<double>));
  double *S_MVDR_dB = (double *)malloc(len_dth * sizeof(double));
  // timestamp start
  timeStart = clock();
  for (int i = 0; i < len_dth; ++i) { // can be paralleled to compute S_MVDR_dB
    for (int j = 0; j < M; ++j) {
      a_vector[j] = exp(I_1 * kc * (std::complex<double>)j * d * sin(dr[i]));
    }
    S_MVDR[i] = compute_S_MUSIC(a_vector, Pn, M);

    // compute S_MVDR_dB
    S_MVDR_dB[i] = 20 * log10(abs(S_MVDR[i]));
  }
  // find Max and position
  double max_temp = S_MVDR_dB[0];
  int position = 0;
  for (int i = 0; i < len_dth; ++i) {
    if (S_MVDR_dB[i] > max_temp) {
      max_temp = S_MVDR_dB[i];
      position = i;
    }
  }
  printf("position : %d\n", position);
  // timestamp end
  timeEnd = clock();
#ifdef PRINT_RESULT
  std::cout << std::setprecision(6) << "Array pattern (cpu):\t" << (timeEnd - timeStart) / CLOCKS_PER_SEC * 1000 << " ms" << std::endl;
  printf("\n--Result--\n");
  std::cout << "Theta estimation:\t" << dth[position] << std::endl;
  std::cout << std::endl << "-----------------------------------------" << std::endl << std::endl;
#endif
  float error;
  error = abs(dth[position] - t_theta[0].real());

  if (error > result[0])
    result[0] = error;
  if (error != 0)
    result[1] += pow(error, 2);

    // plot the result
#ifdef PLOT_RESULT
// std::vector<double> S_MVDR_dB_vec(S_MVDR_dB, S_MVDR_dB + len_dth);
// std::vector<double> dth_vec(dth, dth + len_dth);
// plt::plot(dth_vec, S_MVDR_dB_vec, "blue");
// plt::title("MUSIC DOA Estimation");
// plt::xlabel("Theta (degree)");
// plt::ylabel("Power Spectrum (dB)");
// plt::xlim(dth[0], dth[len_dth - 1]);
// plt::grid(true);
// plt::show();
#endif

  // free memory
  free(t_theta);
  // free(A_theta);
  // free(t_sig);
  free(sig_co);
  free(x_r);
  free(R_xx);
  free(Ve);
  free(De);
  // free(vet_noise);
  free(Pn);
  free(dth);
  free(dr);
  free(a_vector);
  free(S_MVDR);
  free(S_MVDR_dB);
}

extern "C" void origin_doa_function(float IQ_real, float IQ_imag)
{
  //-------------------------------------------------------------------
  // Parameter initialize
  float time_MUSIC = 0.0;
  float time_MVDR = 0.0;
  float timeMusic_start, timeMusic_end; // Total MUSIC Algorithm time
  float timeMVDR_start, timeMVDR_end; // Total MVDR Algorithm time

  struct timeval time_MUSIC_start, time_MUSIC_end, time_MUSIC_diff; // MUSIC time initial
  struct timeval time_MVDR_start, time_MVDR_end, time_MVDR_diff; // MVDR time initial

  //-------------------------------------------------------------------
  int M = 64;
  int snr = 40;
  int qr_iter = 10;
  float result[3] = {0};
  int angle = 50;
  int iter = 1;
  // MUSIC_DOA_1D_CPU_test(M, qr_iter, angle, result, snr);
  // MVDR_DOA_1D_CPU_test(M, qr_iter, angle, result, snr);

  //=================== MUSIC Algorithm =================================
  timeMusic_start = clock();
  printf(L_RED "\n------------------------------\n");
  printf("Original MUSIC cpp\n");
  printf("------------------------------\n" CLOSE);
  MUSIC_DOA_1D_CPU_test(M, qr_iter, angle, result, snr);
  timeMusic_end = clock();
  printf("--------------------------------------\n");
  std::cout << std::setprecision(6) << L_GREEN "Total MUSIC time: \t\t" << (timeMusic_end - timeMusic_start) / CLOCKS_PER_SEC * 1000 << "(ms)" CLOSE << std::endl;
  // gNB->ulsch_doa_MUSIC_time_stats = total_multiply_time / 1000;
  printf(L_GREEN "Total scalar of multiply time :\t%.3f(ms)\n" CLOSE, total_multiply_time / 1000);
  total_multiply_time = 0; // set to 0
  //=====================================================================

  //=================== MVDR Algorithm ==================================
  timeMVDR_start = clock();
  printf(L_RED "\n------------------------------\n");
  printf("Original MVDR cpp\n");
  printf("------------------------------\n" CLOSE);
  MVDR_DOA_1D_CPU_test(M, qr_iter, angle, result, snr);
  timeMVDR_end = clock();
  printf("--------------------------------------\n");
  std::cout << std::setprecision(6) << L_GREEN "Total MVDR time: \t\t" << (timeMVDR_end - timeMVDR_start) / CLOCKS_PER_SEC * 1000 << "(ms)" CLOSE << std::endl;
  // gNB->ulsch_doa_MVDR_time_stats = total_multiply_time / 1000;
  printf(L_GREEN "Total scalar of multiply time :\t%.3f(ms)\n" CLOSE, total_multiply_time / 1000);
  total_multiply_time = 0; // set to 0
  //=====================================================================
}
